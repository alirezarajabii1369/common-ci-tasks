#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

yq --version
jb --version
go version
yarn -v
ruby --version
