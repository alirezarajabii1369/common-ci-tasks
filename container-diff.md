
# [`container-diff`](./container-diff.yml)

Runs [`container-diff`](https://github.com/GoogleContainerTools/container-diff) to compare a CI image to another image.

This can help to determine how much a container image has changed in size due to the current merge request.

```yaml
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.8.0  # renovate:managed
    file: 'container-diff.yml'
    inputs:
      job_name: container-diff
      compare_from_image: $CI_REGISTRY_IMAGE/base:main
      compare_to_image: $CI_REGISTRY_IMAGE/base:${CI_COMMIT_REF_SLUG}
      needs: kaniko build
      stage: docker_build
```
