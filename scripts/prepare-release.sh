#!/usr/bin/env bash

# This script updates all self version references in
# the common-ci-tasks project in preparation for the next release

set -euo pipefail
IFS=$'\n\t'

next_version=$1

inline_sed() {
  if [[ $OSTYPE == 'darwin'* ]]; then
    sed -i '' "$@"
  else
    sed -i "$@"
  fi
}

for i in *.md templates/*.md *.yml templates/*.yml; do
  inline_sed -E 's/ref: v[[:digit:]]+\.[[:digit:]]+.[[:digit:]]+ +# *renovate:managed/ref: v'"$next_version"'  # renovate:managed/' "$i"

  inline_sed -E 's#registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks/([[:alpha:]]+):v[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+#registry.gitlab.com/gitlab-com/gl-infra/common-ci-tasks/\1:v'"$next_version"'#' "$i"
done
